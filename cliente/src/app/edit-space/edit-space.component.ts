import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {Component, ElementRef, ViewChild, OnInit, NgZone, EventEmitter, Output, AfterViewInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router'; // NEW
import { SpaceService } from '../_services/space.service'; // NEW
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material';
import { UploadService } from '../_services/upload.service';
import { MapsService } from '../_services/maps.service';
import { AutocompleteComponent } from './google-places.component';

@Component({
  selector: 'app-edit-space',
  templateUrl: './edit-space.component.html',
  styleUrls: ['./edit-space.component.css']
})
export class EditSpaceComponent implements OnInit, AfterViewInit {
  @ViewChild('mapRef', {static: false }) mapElement: ElementRef;
  @Output() mapRefLoaded: EventEmitter<boolean> = new EventEmitter();

  space: any; // Usando a mesma variável para listar dados e enviar após edição, observar parâmetro image
  idSpace: any;
  public images: Array<any>;

  public condition: Boolean;

  // teste
  resetImg: Array<any>;

  // Controla mapa
  isActive: Boolean = false;
  map: any;
  marker: any;

  // Tags complete
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: string[];
  allFruits: string[] = ['Aniversários', 'Casamentos', 'Corporativos', 'Reuniões', 'Formaturas', 'Festa particular', 'Baladas', 'Religiosos', 'Empresariais', 'Governamentais', 'Universitários', 'Beneficentes'];

  @ViewChild('fruitInput', {static: false}) fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

  constructor(public zone: NgZone, public uploadService: UploadService, public mapsService: MapsService, public dialog: MatDialog, private route: ActivatedRoute, private router: Router, private spaceService: SpaceService) {
    //this.reset();

    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
        startWith(null),
        map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
  }

  ngAfterViewInit(){
    this.mapRefLoaded.emit(true);
}

  public openUploadDialog() {
    this.uploadService.edit_space = true;
    sessionStorage.setItem('idSpace', this.idSpace);

    let dialogRef = this.dialog.open(DialogComponent, { width: '50%', height: '50%' });
  }

  ngOnInit() {

    this.spaceService.updateResultList(true);

    this.resetImg = [];
    this.space = {};
    this.images = [];

    // BUSCAR ESPAÇO PELO ID
    this.route.paramMap.subscribe(params => {

      // ID DO ESPAÇO
      this.idSpace = parseInt(params.get('spaceId'));

      // INFORMAÇÕES
      this.spaceService.listar(this.idSpace).subscribe(data => {
        this.space = data;
        this.fruits = this.space.type;

        if(this.space.lat != null && this.space.lng != null)
        {
          console.log("Armazenar posição e passar para o marcador!");
        }
      });

      // IMAGENS
      this.spaceService.getImages(this.idSpace).subscribe(data => {
        this.images = data
        if(this.images.length == 0)
        {
          this.condition = false;
        } else{
          this.condition = true;
        }
        console.log(this.images);
      });

    });
  }

  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim()) {
        this.fruits.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.fruitCtrl.setValue(null);
    }
  }

  remove(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.allFruits.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }

  submitForm(frm: FormGroup)
  {
    this.route.paramMap.subscribe(() => {
      this.spaceService.update(this.idSpace, this.space).subscribe( data  => {
        console.log("Espaço atualizado com sucesso!", data);
        this.router.navigate(['manage_spaces']);
      },
      error  => {
          console.log("Error", error);
        }
      );

    });
  }

  close() {
    this.images.splice(this.images.indexOf(0), 1);
    console.log(this.images);
  }

  reset() {

    // OBTER DADOS
    this.spaceService.getImages(this.idSpace).subscribe(data => {
      this.resetImg = data
    });

    // Atribuir novamente
    this.images = Array.from(this.resetImg);
  }

  public open()
  {

    //this.renderMap();

    // Buscar elemento pai
    var elemento_pai = document.getElementById('body_map');


    if(this.mapElement == undefined)
    {
      console.log("Ainda não");
    } else {
      this.renderMap();
    }

    // Criar elemento
    //var mapa = document.createElement('h1');

    // Criar o nó de texto
    //var texto = document.createTextNode("Um título qualquer");

    // Anexar o nó de texto ao elemento h1
    //mapa.appendChild(texto);

    // Agora sim, inserir (anexar) o elemento filho (titulo) ao elemento pai (body)
    //elemento_pai.appendChild(mapa);

    /*if(!window.document.getElementById('google-map-script')) {
      var s = window.document.createElement("script");
      s.id = "google-map-script";
      s.type = "text/javascript";
      s.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDiGTLiKNW40Hcp9OEEFZ6lNZnD2U0zEGs&callback=initMap";

      window.document.body.appendChild(s);
    }*/
  }

  // Carrega as dependências do mapa
  loadMap = () => {

    var pos;
    if(this.space.lat != null && this.space.lng != null)
    {
      pos = { lat: this.space.lat, lng: this.space.lng };
    }

    var geolocation = new google.maps.LatLng(-2.422730, -54.736230);

    if(this.space.lat != null && this.space.lng != null) {
      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 15,
        center: pos,
        mapTypeControl: false
      });
    } else {
      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 15,
        center: geolocation,
        mapTypeControl: false
      });
    }

    if(this.space.lat != null && this.space.lng != null){
      // Adiciona o marcador se já estiver na base de dados (Se o espaço já tiver um marcador)
      this.marker = new google.maps.Marker({
        draggable: true,
        map: this.map,
        animation: google.maps.Animation.DROP,
        position: pos
      });
    }

    var geocoder = new google.maps.Geocoder();
    // Quando o botão "Aplicar" é clicado!
    document.getElementById('submit').addEventListener('click', () => {
      this.geocodeAddress(geocoder, this.map);
    });
  }

  // Salvar marcador na base de dados
  saveMarker()
  {
    if(this.marker == null)
    { alert("Selecione uma posição no mapa!"); }
    else {

      // Salvar na base de dados
      var coordinates = {};
      coordinates["lat"] = this.getLatLng(this.marker).lat;
      coordinates["lng"] = this.getLatLng(this.marker).lng;

      this.route.paramMap.subscribe(() => {
        this.spaceService.update(this.idSpace, coordinates).subscribe( data  => {
          console.log("Espaço atualizado com sucesso!", data);
          this.router.navigate(['manage_spaces']);
        },
        error  => {
            console.log("Error", error);
          }
        );

      });
    }
  }

  getLatLng(marker)
  {
    return ({lat: marker.getPosition().lat(), lng: marker.getPosition().lng()});
  }

  // Obtém coordenadas pelo endereço passado
  geocodeAddress(geocoder, resultsMap) {
    var address = (<HTMLInputElement>document.getElementById('address')).value;
    geocoder.geocode({'address': address}, (results, status) => {
      if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);

        // Remove marcador atual
        if(this.marker != null)
        {
          this.marker.setMap(null);
        }

        this.marker = new google.maps.Marker({
          draggable: true,
          map: resultsMap,
          animation: google.maps.Animation.DROP,
          position: results[0].geometry.location
        });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }

  // Desenha o mapa
  renderMap() {
    this.loadMap();
  }

  // Autocompleta endereço digitado
  getSpaceAddress(place: object) {
    this.mapsService.getEstablishmentAddress(place);
  }

}
