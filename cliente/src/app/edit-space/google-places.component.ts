import { Component, ViewChild, EventEmitter, Output, OnInit, AfterViewInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import {} from 'googlemaps';
import { SpaceService } from '../_services/space.service';

@Component({
    selector: 'AutocompleteComponent',
    template: `
    <div class="input-group">
    <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fas fa-map-marker-alt"></i></span>
              </div>
  <input class="form-control input"
        id="address"
        type="text"
        [(ngModel)]="autocompleteInput"
        aria-describedby="basic-addon1"
        #addresstext
        >
        <div class="input-group-append" *ngIf="isEditSpace">
        <input class="btn btn-outline-secondary" id="submit" type="button" value="Aplicar">
      </div>
        </div>
    `,
})

export class AutocompleteComponent implements AfterViewInit {
    @Input() adressType: string;
    @Output() setAddress: EventEmitter<any> = new EventEmitter();
    @ViewChild('addresstext', {static: false}) addresstext: any;

    autocompleteInput: string;
    queryWait: boolean;

    public isEditSpace: Boolean;

    constructor(private spaceService: SpaceService) { }

    ngOnInit() {
      this.spaceService.resultList$.subscribe(resultList => {
        console.log("Valor foi alterado: " + resultList);
        this.isEditSpace = resultList;
      });
    }

    ngAfterViewInit() {
      this.getPlaceAutocomplete();
    }

    private getPlaceAutocomplete() {
        const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
          {
            componentRestrictions: { country: 'BR' },
            types: [this.adressType]  // 'establishment' / 'address' / 'geocode'
          });

        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            const place = autocomplete.getPlace();
            this.invokeEvent(place);
        });
    }

    invokeEvent(place: Object) {
        this.setAddress.emit(place);
    }

}
