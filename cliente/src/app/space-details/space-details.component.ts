import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SpaceService } from '../_services/space.service';

@Component({
  selector: 'app-space-details',
  templateUrl: './space-details.component.html',
  styleUrls: ['./space-details.component.css']
})
export class SpaceDetailsComponent {

  space;
  images: Array<any>;

  constructor(private route: ActivatedRoute, private spaceService: SpaceService) { }

  ngOnInit() {
    // BUSCAR ESPAÇO PELO ID
    this.route.paramMap.subscribe(params => {
      this.spaceService.spaceId = parseInt(params.get('spaceId')); // OBTÉM DO ID DO ESPAÇO
      sessionStorage.setItem("spaceId", this.spaceService.spaceId.toString());
      this.spaceService.listar(this.spaceService.spaceId).subscribe(data => this.space = data);

      // IMAGES
      this.spaceService.getImages(parseInt(params.get('spaceId'))).subscribe(data => {
        this.images = data
        console.log(this.images);
        //console.log("VERIFICAÇÃO: " + this.images[0]);
      });
    });
  }

}
