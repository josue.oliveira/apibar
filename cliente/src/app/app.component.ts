import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import socketio from 'socket.io-client';

import { AuthenticationService } from './_services';
import { User } from './_models';

import {Globals} from './globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    public routeLoading: boolean = false;
    currentUser: User;

    constructor(public globals: Globals, private router: Router, private authenticationService: AuthenticationService ) {

      this.router.events.subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.routeLoading = true;
        }

        if (event instanceof NavigationEnd ||
          event instanceof NavigationCancel ||
          event instanceof NavigationError)
        {
            this.routeLoading = false;
        }
      });

      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    ngOnInit() {
      const userId = JSON.parse(localStorage.getItem("currentUser")).id;
      const socket = socketio('http://localhost:3000', {
        query: { userId },
      });

      socket.on('booking_request', data => {
        this.globals.bookingsRequests.push(data);
      });

      socket.on('booking_response', booking => {
        alert(`Sua reserva em ${booking.space.name} no período de ${booking.check_in} a ${booking.check_out} foi ${booking.status ? 'APROVADA': 'REJEITADA'}`);
        this.globals.bookingsResponse.push(booking);
      });
    }

    logout() {
      this.authenticationService.logout();
      this.router.navigate(['/login']);
    }
}
