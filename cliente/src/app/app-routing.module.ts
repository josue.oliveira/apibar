import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { AuthGuard } from './_helpers';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { SpaceDetailsComponent } from './space-details/space-details.component';
import { SpaceRegistrationComponent } from './space-registration/space-registration.component';
import { Error404Component } from './error404/error404.component';
import { AboutComponent } from './about/about.component';
import { UploadImageComponent } from './space-registration/upload-image/upload-image.component'; // NEW
import { AccountComponent } from './account/account.component';
import { ProfileComponent } from './profile/profile.component';
import { BookComponent } from './book/book.component';
import { ManageSpacesComponent } from './manage-spaces/manage-spaces.component';
import { EditSpaceComponent } from './edit-space/edit-space.component';
import { ChatComponent } from './chat/chat.component';
import { BookingComponent } from './booking/booking.component';
import { BookingDetailsComponent } from './booking-details/booking-details.component';
import { BookingRequestsComponent } from './booking-requests/booking-requests.component';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'about', component: AboutComponent },
    { path: 'chat', component: ChatComponent },
    { path: 'login', component: LoginComponent },
    { path: 'registration', component: UserRegistrationComponent },
    { path: 'spaces/:spaceId', component: SpaceDetailsComponent, canActivate: [AuthGuard] },
    { path: 'spaces/:spaceId/book', component: BookComponent, canActivate: [AuthGuard] },
    { path: 'manage_spaces/newspace', component: SpaceRegistrationComponent, canActivate: [AuthGuard] },
    { path: 'manage_spaces/newspace/upload_image', component: UploadImageComponent, canActivate: [AuthGuard] },
    { path: 'account', component: AccountComponent, canActivate: [AuthGuard] },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
    { path: 'manage_spaces', component: ManageSpacesComponent, canActivate: [AuthGuard] },
    { path: 'manage_spaces/:spaceId', component: EditSpaceComponent, canActivate: [AuthGuard] },
    { path: 'booking', component: BookingComponent, canActivate: [AuthGuard] },
    { path: 'booking/:bookId', component: BookingDetailsComponent, canActivate: [AuthGuard] },
    { path: 'requests', component: BookingRequestsComponent, canActivate: [AuthGuard] },
    { path: '**', component: Error404Component },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
