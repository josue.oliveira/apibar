import { Component, OnInit } from '@angular/core';
import { NgbDatepickerConfig, NgbDateStruct, NgbDate, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';
import { SpaceService } from '../_services/space.service';
import { ActivatedRoute, Router } from '@angular/router'; // NEW

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})

export class BookComponent implements OnInit {

  model: NgbDateStruct;

  // Data selecionada
  date: {year: number, month: number};

  hoveredDate: NgbDate;
  fromDate: NgbDate; // Check-in
  toDate: NgbDate; // Check-out
  days: number; // Número de dias selecionados
  price: number; // Total a ser pago

  // EXPRESSÃO REGULAR PARA TRATAR DATA
  //matches = /(\d{1,2})[\/](\d{1,2})[\/](\d{4})/;
  matches = /(\d{4})[\-](\d{1,2})[\-](\d{1,2})/;

  // DADOS DO ESPAÇO
  space: any;
  // DADOS DA RESERVA
  book = {
    check_in: null,
    check_out: null,
    totalPaid: null,
    status: false,
    userId: null,
    spaceId: null
  }
  // Desabilitar dias selecionados
  isDisabled: any;
  // Datas para desabilitar
  disabledDates: NgbDateStruct[] = [];

  constructor(private router: Router, config: NgbDatepickerConfig, private calendar: NgbCalendar, private spaceService: SpaceService) {
    this.fromDate = calendar.getToday(); // Seleciona a data atual
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 1); // Seleciona 1 dia após a data atual

    // customize default values of datepickers used by this component tree
    config.minDate = calendar.getToday();
    config.maxDate = {year: 2021, month: 12, day: 31};

    // weekends are disabled
    // config.markDisabled = (date: NgbDate) => calendar.getWeekday(date) >= 6;
  }

  ngOnInit() {

    //alert("Não é possível selecionar o período específicado!!");

    // BUSCA OS DADOS DO ESPAÇO QUE ESTÁ SENDO RESERVADO
    this.spaceService.listar(parseInt(sessionStorage.getItem("spaceId"))).subscribe(data => {
      console.log(data);
      this.space = data;
    });

    // BUSCA RESERVAS FEITAS PELOS USUÁRIOS PARA UM DETERMINADO ESPAÇO
    this.spaceService.bookBySpace(parseInt(sessionStorage.getItem("spaceId"))).subscribe(data => {

      data.forEach( element => {

        var getDateIn = this.matches.exec(element.check_in);
        var getDateOut = this.matches.exec(element.check_out);

        if(this.matches == null)
        { return false; } else{

          // Date In
          this.disabledDates.push({
            year: Number(getDateIn[1]),
            month: Number(getDateIn[2]),
            day: Number(getDateIn[3])
          });

          // Se check_out for null, não é preciso obtê-lo, nem calcular o período entre datas
          if(!(element.check_out == null))
          {
            // Date Out
            this.disabledDates.push({
              year: Number(getDateOut[1]),
              month: Number(getDateOut[2]),
              day: Number(getDateOut[3])
            });

            // Intervalo entre datas
            this.getPeriod(
              this.calendar,
            [{
                year: Number(getDateIn[1]),
                month: Number(getDateIn[2]),
                day: Number(getDateIn[3])
              }],
              [{
                year: Number(getDateOut[1]),
                month: Number(getDateOut[2]),
                day: Number(getDateOut[3])
              }]
              ).forEach(element => {
              this.disabledDates.push(element);
            });
          }
        }
      })

      // Desabilita dias selecionados
      this.isDisabled = (date: NgbDateStruct, current: {month: number,year: number})=> {
        return this.disabledDates.find(x => NgbDate.from(x).equals(date))? true: false;
      }

      // Get Days
      this.days = this.getDays();

      // Calcula valor a ser pago
      this.price = this.calcularPreco(this.days, this.space.price);
    });
  }

  // Seleciona o período especificado
  onDateSelection(date: NgbDate) {

    // Controla a busca por reservas em um período selecionado
    var isValid = false;

    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;

      // SE EXISTIR DATAS DESATIVADAS NO PERÍODO SELECIONADO, EXIBIR UM ALERT E CANCELAR A SELEÇÃO
      var isValidSelection = this.getPeriod(this.calendar, [{
        year: Number(this.fromDate.year),
        month: Number(this.fromDate.month),
        day: Number(this.fromDate.day)
      }],
      [{
        year: Number(this.toDate.year),
        month: Number(this.toDate.month),
        day: Number(this.toDate.day)
      }]
      );

      // Faz uma busca para verificar se as datas selecionadas são válidas
      for (let index = 0; index < isValidSelection.length; index++) {
        const elementA = isValidSelection[index];

        for (let index = 0; index < this.disabledDates.length; index++) {
          const elementB = this.disabledDates[index];

          // If existe uma data selecionada inválida dentro do período específicado, cancelar seleção e exibir um alert
          if(this.isEquivalent(elementA, elementB))
          {
            alert("Não é possível selecionar o período específicado, pois ele contém reservas!!");
            isValid = true;
            // Cancela a seleção do período especificado
            this.toDate = null;
            break;
          }

        }

        // se já encontrou alguma reserva no período, quebra a execução do laço for
        if(isValid)
        { break; }
      }

    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    // Get Days
    this.days = this.getDays();

    // Calcula valor a ser pago
    this.price = this.calcularPreco(this.days, this.space.price);
  }

  // Calcula o número de dias selecionados
  getDays()
  {
    return this.toDate == null ? 1 : Math.round((new Date(this.toDate.year+'-'+this.toDate.month+'-'+this.toDate.day).getTime() -
    new Date(this.fromDate.year+'-'+this.fromDate.month+'-'+this.fromDate.day).getTime())/(24*60*60*1000)) + 1;
  }

  // Retorna todas as datas dentro de um intervalo
  getPeriod(calendar: NgbCalendar, fromDate: NgbDateStruct[], toDate: NgbDateStruct[])
  {
    var range: NgbDateStruct[] = []; // Armazena o período de datas
    var flag: Boolean = true;

    var count_day, count_month, count_year, count: Number;

    count_day = 1;
    count_month = 0;
    count_year = 0;
    var date: NgbDate;

    while(flag)
    {
      date = new NgbDate(fromDate[0].year, fromDate[0].month+count_month, fromDate[0].day+count_day);
      if(calendar.isValid(date))
      {
        // console.log("DATA: " + (fromDate[0].day+count_day)+"/"+(fromDate[0].month+count_month)+"/"+fromDate[0].year);
        if(
          ((fromDate[0].day+count_day)+"/"+(fromDate[0].month+count_month)+"/"+(fromDate[0].year+count_year))
          == toDate[0].day+"/"+toDate[0].month+"/"+toDate[0].year)
        {
          flag = false;
        }
        else{
          range.push({
            year: fromDate[0].year+count_year,
            month: fromDate[0].month+count_month,
            day: fromDate[0].day+count_day
          })
        }
        count_day += 1;
      }
      else{

        // Vai para o próximo mês
        count_month += 1;

        // Inicia no dia 1 do próximo mês
        fromDate[0].day = 1;
        count_day = 0;

        // Salva data atual para verificar mudança de ano
        date = new NgbDate(fromDate[0].year, fromDate[0].month+count_month, fromDate[0].day+count_day);

        // Se a data não for válida, recebe um novo ano e começa a contar a partir do mês de janeiro
        if(!calendar.isValid(date))
        {
          // Vai para o próximo ano
          count_year += 1;
          // Inicia no mês de janeiro
          fromDate[0].month = 1;
          // Contagem do mês é zerada
          count_month = 0;
          // Inicia no dia 1 de janeiro
          fromDate[0].day = 1;
          // Contagem dos dias é zerada
          count_day = 0;
        }
      }
    }

    return range;
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  // Calcula o preço da reserva
  calcularPreco(days, price)
  {
    return days * price;
  }

  // Verifica se dois objetos são iguais
  isEquivalent(objectA, objectB) {
    // Create arrays of property names
    var aProps = Object.getOwnPropertyNames(objectA);
    var bProps = Object.getOwnPropertyNames(objectB);

    // If number of properties is different,
    // objects are not equivalent
    if (aProps.length != bProps.length) {
        return false;
    }

    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];

        // If values of same property are not equal,
        // objects are not equivalent
        if (objectA[propName] !== objectB[propName]) {
            return false;
        }
    }

    // If we made it this far, objects
    // are considered equivalent
    return true;
  }

  // Efetua a reserva de determinado espaço
  reservar()
  {
    // PEGA TODOS OS DADOS
    if(this.toDate == null)
    {
      this.book.check_in = this.fromDate.year+"-"+this.fromDate.month+"-"+this.fromDate.day;
    }
    else{
      this.book.check_in = this.fromDate.year+"-"+this.fromDate.month+"-"+this.fromDate.day;
      this.book.check_out = this.toDate.year+"-"+this.toDate.month+"-"+this.toDate.day;
    }

    this.book.totalPaid = this.price;
    this.book.userId = JSON.parse(localStorage.getItem("currentUser")).id;
    this.book.spaceId = this.space.id;

    this.spaceService.book(this.book).subscribe(data => {
      console.log(data);
      console.log("Solicitação de reserva enviada!");
    })

    this.router.navigate(['booking']);
  }

}
