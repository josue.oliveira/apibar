import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ActivatedRoute } from '@angular/router';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: any;
  form: FormGroup;
  _picture: any; // DEFAULT PROFILE IMAGE

  constructor(private http: HttpClient, public fb: FormBuilder, private userService: UserService, private route: ActivatedRoute) {
    this.form = this.fb.group({
      name: [''],
      avatar: [null]
    })
  }

  ngOnInit() {

    // CONFIGURA A FOTO DE PERFIL PADRÃO
    this._picture = "https://myrealdomain.com/images/generic-avatar-4.png";

    this.route.paramMap.subscribe(() => {
      this.userService.getById(JSON.parse(localStorage.getItem("currentUser")).id).subscribe(data => {
        this.user = data;

        // VERIFICA SE O USUÁRIO TEM UMA FOTO DE PERFIL
        // SE NÃO, MANTÉM A IMAGEM PADRÃO
        if(this.user.profile_picture != null)
        {
          this._picture = this.user.profile_picture;
        }

        console.log(data);
      });
    });
  }

  uploadFile(event){
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      avatar: file
    });
    this.form.get('avatar').updateValueAndValidity()
  }

  submitForm(){
    console.log(this.form.value);
    var formData: any = new FormData();

    // KEY : VALUE
    formData.append('imageName', 'Nome da imagem');
    formData.append("cloudImage", this.form.get('avatar').value);
    formData.append("userId", JSON.parse(localStorage.getItem("currentUser")).id);

    this.route.paramMap.subscribe(() => {
      this.userService._change_profile_picture(formData)
      .subscribe(
        (response) => console.log(response),
        (error) => console.log(error)
      )
    });
  }

}
