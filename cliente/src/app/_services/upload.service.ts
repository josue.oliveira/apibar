import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpRequest,
  HttpEventType,
  HttpResponse
} from '@angular/common/http';
import { Subject, Observable } from 'rxjs';

import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  public edit_space: Boolean;
  public create_space: Boolean;

  constructor(private http: HttpClient) {
    this.edit_space = false;
    this.create_space = false;
  }

  public upload( files: Set<File>, method: string ): { [key: string]: { progress: Observable<number> } } {
    // this will be the our resulting map
    const status: { [key: string]: { progress: Observable<number> } } = {};

    files.forEach(file => {
      // create a new multipart-form for every file
      const formData: FormData = new FormData();

      formData.append('imageName', 'Nome da imagem');
      formData.append('cloudImage', file, file.name);
      formData.append('spaceId', sessionStorage.getItem('id_space_creation'));

      // create a http-post request and pass the form
      // tell it to report the upload progress

      // VER COMO MODIFICAR ISSO
      const req = new HttpRequest(method, `${environment.apiUrl}/upload/space_picture`, formData, {
        reportProgress: true
      });

      // create a new progress-subject for every file
      const progress = new Subject<number>();

      // send the http-request and subscribe for progress-updates

      const startTime = new Date().getTime();
      this.http.request(req).subscribe(event => {
        
        if (event.type === HttpEventType.UploadProgress) {
          // calculate the progress percentage

          const percentDone = Math.round((100 * event.loaded) / event.total);
          // pass the percentage into the progress-stream
          progress.next(percentDone);
        } else if (event instanceof HttpResponse) {
          // Close the progress-stream if we get an answer form the API
          // The upload is complete
          progress.complete();
          console.log("Upload image successful!");
        }
      });

      // Save every progress-observable in a map of all observables
      status[file.name] = {
        progress: progress.asObservable()
      };
    });

    // return the map of progress.observables
    return status;
  }

  // UPLOAD DE IMAGENS POR ESPAÇO
  public uploadBySpace( files: Set<File>, method: string, spaceId: string ): { [key: string]: { progress: Observable<number> } } {
    // this will be the our resulting map
    const status: { [key: string]: { progress: Observable<number> } } = {};

    files.forEach(file => {
      // create a new multipart-form for every file
      const formData: FormData = new FormData();

      formData.append('imageName', 'Nome da imagem');
      formData.append('cloudImage', file, file.name);
      formData.append('spaceId', spaceId);

      // create a http-post request and pass the form
      // tell it to report the upload progress

      // VER COMO MODIFICAR ISSO
      const req = new HttpRequest(method, `${environment.apiUrl}/images/uploads`, formData, {
        reportProgress: true
      });
      sessionStorage.removeItem('idSpace'); // Remove o id do espaço

      // create a new progress-subject for every file
      const progress = new Subject<number>();

      // send the http-request and subscribe for progress-updates

      const startTime = new Date().getTime();
      this.http.request(req).subscribe(event => {
        
        if (event.type === HttpEventType.UploadProgress) {
          // calculate the progress percentage

          const percentDone = Math.round((100 * event.loaded) / event.total);
          // pass the percentage into the progress-stream
          progress.next(percentDone);
        } else if (event instanceof HttpResponse) {
          // Close the progress-stream if we get an answer form the API
          // The upload is complete
          progress.complete();
          console.log("Upload image successful!");
        }
      });

      // Save every progress-observable in a map of all observables
      status[file.name] = {
        progress: progress.asObservable()
      };
    });

    // return the map of progress.observables
    return status;
  }

}
