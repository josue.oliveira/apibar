import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpaceService {

  public spaceId: Number;

  // NEW
  private resultList: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(null);
  public resultList$: Observable<Boolean> = this.resultList.asObservable();

  constructor(private http: HttpClient) {
    this.spaceId = null;
  }

  updateResultList(updatedList) {
    this.resultList.next(updatedList);
  }

  listar(params){
    return this.http.get<any[]>(`${environment.apiUrl}/content/spaces/`+params);
  }

  getByOwner(params){
    return this.http.get<any[]>(`${environment.apiUrl}/owner/spaces/`+params);
  }

  create(space: any){
    return this.http.post(`${environment.apiUrl}/content/spaces/`, space);
  }

  update(id, data: any){
    return this.http.put(`${environment.apiUrl}/content/spaces/`+id, data);
  }

  getImages(params){ // Obtém as imagens do espaço
    console.log("VER -> " + params);
    return this.http.get<any[]>(`${environment.apiUrl}/images/space/`+params);
  }

  // Reservas
  book(book: any){
    return this.http.post(`${environment.apiUrl}/customers/locations`, book);
  }

  bookBySpace(params){ // Passar o id do espaço
    return this.http.get<any[]>(`${environment.apiUrl}/space/bookings/`+params);
  }

  booking(params){
    return this.http.get<any[]>(`${environment.apiUrl}/customers/bookings/`+params);
  }

  oneBook(params, data: HttpHeaders){
    return this.http.get<any[]>(`${environment.apiUrl}/customers/book/`+params, {headers: data});
  }

  deleteBook(params){
    return this.http.delete(`${environment.apiUrl}/customers/locations/`+params);
  }

  filterSpaces(params){
    return this.http.get<any[]>(`${environment.apiUrl}/filter/spaces`, {
      params,
      observe: 'response'
    });
  }

  approved(data, params)
  {
    return this.http.put(`${environment.apiUrl}/spaces/${params}/approvals`, data);
  }

  rejected(data, params)
  {
    return this.http.put(`${environment.apiUrl}/spaces/${params}/rejections`, data);
  }

}
