﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { User } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id) {
        return this.http.get<User[]>(`${environment.apiUrl}/users/` + id);
    }

    update(id, data: any){
        return this.http.put(`${environment.apiUrl}/users/` + id, data);
    }

    create(space: any){
        return this.http.post(`${environment.apiUrl}/users`, space);
    }

    _change_profile_picture(data: any){
        return this.http.put(`${environment.apiUrl}/upload/profile_picture`, data);
    }
}