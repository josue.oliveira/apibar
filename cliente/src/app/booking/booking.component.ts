import { Component, OnInit } from '@angular/core';
import { SpaceService } from '../_services/space.service';
import { element } from 'protractor';
import { Globals } from '../globals';

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.css']
})
export class BookingComponent implements OnInit {

  bookings: Array<any>;
  activedBookings: any = [];
  situacao: Boolean;
  public possuiReserva: Boolean;
  public isCheckOut: Boolean;

  date = /(\d{4})[\-](\d{1,2})[\-](\d{1,2})/; // Formatar data

  constructor(private globals: Globals, private spaceService: SpaceService) {
    this.situacao = false;
  }

  ngOnInit() {

    var check_in;
    var check_out;

    // window.location.reload(); // Refresh page
    this.spaceService.booking(JSON.parse(localStorage.getItem("currentUser")).id).subscribe(data => {

      if(data.length == 0)
      {
        this.possuiReserva = false;
      } else{
        this.possuiReserva = true;

        // Reservas ativas
        data.forEach( element => {
          if(element.status)
          {
            this.activedBookings.push(element);
          }
        });

        // FORMATA DADOS
        data.forEach( (element, index) => {

          // CHECK-IN
          check_in = this.date.exec(element.check_in);
          data[index].check_in = check_in[3]+"/"+check_in[2]+"/"+check_in[1];

          if(element.status)
          {
            element.status = "ativa";
          }
          if(!element.check_out)
          {
            this.isCheckOut = false;
          } else {
            // CHECK-OUT
            check_out = this.date.exec(element.check_out);
            data[index].check_out = check_out[3]+"/"+check_out[2]+"/"+check_out[1];
            this.isCheckOut = true;
          }
        });

        this.bookings = data;
      }
    });
  }

}
