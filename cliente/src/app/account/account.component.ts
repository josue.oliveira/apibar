import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})

export class AccountComponent {

  updateUserForm = new FormGroup({
    name: new FormControl(''),
    lastname: new FormControl(''),
    email: new FormControl(''),
    dob: new FormControl(''),
    address: new FormControl(''),
    gender: new FormControl(''),
    phone: new FormControl(''),
  });

  user: any;

  constructor(private userService: UserService, private route: ActivatedRoute, public fb: FormBuilder) {}

  ngOnInit(){
    // GET PARA OBTER DETALHES DO USUÁRIO ATUAL
    this.route.paramMap.subscribe(() => {
      this.userService.getById(JSON.parse(localStorage.getItem("currentUser")).id).subscribe(data => {
        this.user = data;
        console.log(data);
      });
    });
  }

  submitForm(){
    console.log(this.updateUserForm.value);
    this.userService.update(JSON.parse(localStorage.getItem("currentUser")).id, this.updateUserForm.value).subscribe(res => {
      console.log(res);
    })
  }

}
