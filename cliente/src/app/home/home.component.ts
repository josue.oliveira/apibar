import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { first, debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { SpaceService } from '../_services/space.service';
import {Observable} from 'rxjs';
import {} from 'googlemaps';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '@app/_models';
import { UserService, AuthenticationService } from '@app/_services';
import 'rxjs/add/operator/filter';
import { MapsService } from '../_services/maps.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  private mapElement: ElementRef;

  @ViewChild('mapRef', {static: false }) set content(content: ElementRef){
    this.mapElement = content;
  }

  title = 'Busque um espaço para seu evento';
  loading = false;
  users: User[];
  spaces: Array<any>;

  // MAPS SPACES
  map: any;
  markers: Array<any> = [];
  public searchLocation: any;
  public searchCategory: any;
  public neighborSpace: any = {};
  public neighborAllSpaces: Array<any> = [];

  // Material datepicker
  minDate = new Date(Date.now()); //Desabilita todas as datas até a data de hoje
  // maxDate = new Date();


  isShowingMap: Boolean = false;
  isShowMap: Boolean = false;

  constructor(private route: ActivatedRoute, private mapsService: MapsService, private userService: UserService, private spaceService: SpaceService, private router: Router) { }

  ngOnInit() {

    this.spaceService.updateResultList(false);

    this.markers = [];
    this.loading = true;
    this.userService.getAll().pipe(first()).subscribe(users => {
        console.log(users);
        this.loading = false;
        this.users = users;
    });

    this.listar();
  }

  // Busca os dados de todos os espaços disponíveis
  listar(){
    this.spaceService.listar('').subscribe(data => {
      this.spaces = data;
      console.log(data);


      // Pega dados do espaço para colocar nos marcadores no mapa
      this.spaces.forEach(element => {
        this.neighborSpace["geolocalization"] = {lat: element.lat, lng: element.lng};
        this.neighborSpace["title"] = element.name;
        this.neighborSpace["image_url"] = element.image;

        this.neighborAllSpaces.push(this.neighborSpace);
      });

      // Gancho de vida após executar listar :)
      this.renderMap();
    });
  }

  // Autocompletar tempos
  /*search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.cities.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )*/

  // Carrega as dependências do mapa
  loadMap = () => {

    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({'address': this.searchLocation}, (results, status) => {
      if (status === 'OK') {
        // Configura mapa para posição
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
          center: results[0].geometry.location,
          zoom: 13,
          mapTypeControl: false
        });

        // Adiciona marcadores
        this.clearMarkers();
        for (var i = 0; i < this.spaces.length; i++) {
          this.addMarkerWithTimeout(this.spaces[i], i * 200, this.map, i);
        }
      }
      /**
       * else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
       */
    });
  }

  // Adciona marcadores
  addMarkerWithTimeout(data, timeout, map, index) {
    window.setTimeout(() => {
      this.markers.push(new google.maps.Marker({
        position: {lat: data.lat, lng: data.lng},
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP
      }));

      // Info Windows
      // Adicionar a mesma imagem que aparece no anúncio
      var contentString = '<div id="content">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<img mat-card-image src='+data.image+' style="height:9em; width:18em" alt="Photo of local">'+
      '<h3 id="thirdHeading" class="thirdHeading">'+data.name+'</h3>'+
      '<div id="bodyContent">'+
      '<p>2 recomendações para esse local.</p>'+
      '</div>'+
      '</div>';

      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });

      this.markers[index].addListener('click', () => {
        infowindow.open(map, this.markers[index]);
      });

    }, timeout);
  }

  // Limpa marcadores
  clearMarkers() {
    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(null);
    }
    this.markers = [];
  }

  // Desenha o mapa
  renderMap() {
    this.loadMap();
  }

  // Encontra um objeto pelo campo especificado
  findByField = ( field ) => ( value, list ) =>
  list.find( obj => obj[ field ] === value )

  // Encontra um objeto pelo nome em um array
  findByName = ( value ) => ({
    in: ( list ) => this.findByField( 'city' )( value, list )
  })

  // Refresh component
  refreshComponent()
  {
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/']);
    });
  }

  // Atualiza mapa com pesquisa
  refreshMap()
  {
    this.renderMap();
  }

  getCityAddress(place: object) {
    this.mapsService.getAddress(place);
  }

  filterSpaces()
  {
    var query, allNull;
    // Expressões regulares
    var date = /(\d{1,2})[\/](\d{1,2})[\/](\d{4})/;
    var pattern_location = /,\s*/;

    // Recebe o valor que está no Input da localização
    this.searchLocation = (<HTMLInputElement>document.getElementById('address')).value;
    // Formata dados da localização
    var location = this.searchLocation.split(pattern_location);
    // Formata dados do período selecionado
    var check_in = date.exec((<HTMLInputElement>document.getElementById('check_in')).value);
    var check_out = date.exec((<HTMLInputElement>document.getElementById('check_out')).value);

    // NENHUM FILTRO É FORNECIDO
    if(!this.searchCategory && !this.searchLocation && !check_in && !check_out)
    {
      this.isShowMap = false;
      allNull = true;
    }

    // APENAS A LOCALIZAÇÃO É FORNECIDA
    if(!this.searchCategory && this.searchLocation && !check_in && !check_out)
    {
      this.isShowMap = true;
      console.log("Apenas a localização foi fornecida!");
      query = {
        city: location[0],
        state: location[1],
        country: location[2]
      }
    }

    // APENAS A CATEGORIA É FORNECIDA
    if(this.searchCategory && !this.searchLocation && !check_in && !check_out)
    {
      this.isShowMap = false;
      console.log("Apenas a categoria foi fornecida!");
      query = {
        category: this.searchCategory
      }
    }

    // APENAS O PERÍODO É FORNECIDO
    if(!this.searchCategory && !this.searchLocation && check_in && check_out)
    {
      this.isShowMap = false;
      // Tratar também quando for fornecido um único dia
      console.log("Apenas o período foi fornecido!")
      query = {
        check_in: check_in[3]+"-"+check_in[2]+"-"+check_in[1],
        check_out: check_out[3]+"-"+check_out[2]+"-"+check_out[1]
      }
    }

    // LOCALIZAÇÃO E PERÍODO SÃO FORNECIDOS
    if(!this.searchCategory && this.searchLocation && check_in && check_out)
    {
      this.isShowMap = true;
      console.log("Localização e período foram fornecidos!");
      query = {
        city: location[0],
        state: location[1],
        country: location[2],
        check_in: check_in[3]+"-"+check_in[2]+"-"+check_in[1],
        check_out: check_out[3]+"-"+check_out[2]+"-"+check_out[1]
      }
    }

    // PERÍODO E CATEGORIA SÃO FORNECIDOS
    if(this.searchCategory && !this.searchLocation && check_in && check_out)
    {
      this.isShowMap = false;
      console.log("Período e categoria foram fornecidos!");
      query = {
        category: this.searchCategory,
        check_in: check_in[3]+"-"+check_in[2]+"-"+check_in[1],
        check_out: check_out[3]+"-"+check_out[2]+"-"+check_out[1]
      }
    }

    // LOCALIZAÇÃO E CATEGORIA SÃO FORNECIDOS APENAS
    if(this.searchLocation && this.searchCategory && !check_in && !check_out)
    {
      this.isShowMap = true;
      console.log("Localização e categoria foram fornecidos!");
      query = {
        city: location[0],
        state: location[1],
        country: location[2],
        category: this.searchCategory
      }
    }

    // LOCALIZAÇÃO, PERÍODO E CATEGORIA SÃO FORNECIDOS
    if(this.searchLocation && this.searchCategory && check_in && check_out)
    {
      this.isShowMap = true;
      console.log("OK 2");
      query = {
        city: location[0],
        state: location[1],
        country: location[2],
        category: this.searchCategory,
        check_in: check_in[3]+"-"+check_in[2]+"-"+check_in[1],
        check_out: check_out[3]+"-"+check_out[2]+"-"+check_out[1]
      }
    }

      // SE TODOS OS CAMPOS, RETORNAR TODOS OS ESPAÇOS
      if(allNull)
      {
        this.spaceService.listar('').subscribe(response => {
          this.spaces = response;

          // Pega dados do espaço para colocar nos marcadores no mapa
          this.spaces.forEach(element => {
            this.neighborSpace["geolocalization"] = {lat: element.lat, lng: element.lng};
            this.neighborSpace["title"] = element.name;
            this.neighborSpace["image_url"] = element.image;

            this.neighborAllSpaces.push(this.neighborSpace);
          });

        });

        // Verifica se a pesquisa inclui a localização, se não, esconde o mapa
        if(!this.isShowMap)
        {
          this.isShowingMap = false;
        }
      }
      // SE NÃO, RETORNAR FILTRO
      else{
        this.spaceService.filterSpaces(query).subscribe(response => {
          console.log(response.body);

          this.spaces = response.body;

          // Pega dados do espaço para colocar nos marcadores no mapa
          this.spaces.forEach(element => {
            this.neighborSpace["geolocalization"] = {lat: element.lat, lng: element.lng};
            this.neighborSpace["title"] = element.name;
            this.neighborSpace["image_url"] = element.image;

            this.neighborAllSpaces.push(this.neighborSpace);
          });
        });

        // Verifica se o mapa está sendo exibido e atualiza conforma necessário
        if(this.isShowingMap)
        {
          this.renderMap();
        }

        // Verifica se a pesquisa inclui a localização, se não, esconde o mapa
        if(!this.isShowMap)
        {
          this.isShowingMap = false;
        }
      }

  }

  public mostrarMapa(condition)
  {
    if(!this.isShowingMap)
    {
      this.isShowingMap = true; // Mapa com a referência
      if(this.isShowMap)
      {
        this.renderMap();
      }
    }
    else{ this.isShowingMap = false; }
  }

}
