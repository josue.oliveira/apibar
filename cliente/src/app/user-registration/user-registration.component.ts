import { Component, OnInit } from '@angular/core';

import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@app/_services';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  user: any;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.user = {};
  }

  create(frm: FormGroup){
    this.userService.create(this.user).subscribe( data  => {
        console.log("Usuário cadastrado com sucesso! ", data);
          this.router.navigate(['login']);
        },
        error  => {
        
        console.log("Error", error);
        
        }
    );
    frm.reset();
  }

}
