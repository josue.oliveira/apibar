import { Component, OnInit } from '@angular/core';
import { SpaceService } from '../_services/space.service';
import { Router } from '@angular/router'; // NEW

import {Globals} from '../globals';

@Component({
  selector: 'app-booking-requests',
  templateUrl: './booking-requests.component.html',
  styleUrls: ['./booking-requests.component.css']
})
export class BookingRequestsComponent implements OnInit {

  constructor(public globals: Globals, private spaceService: SpaceService, private router: Router) { }

  ngOnInit() {}

  // ACEITAR UMA SOLICITAÇÃO DE RESERVA
  acceptRequest(id, element)
  {
    this.spaceService.approved({ status: true }, id).subscribe(response => {
      console.log(response);
      this.globals.bookingsRequests.splice(element, 1);
      this.router.navigate(['requests']); // <-- REMOVE ELEMENTO SE A SOLICITAÇÃO FOR ACEITA
    });
  }

  // REJEITAR UMA SOLICITAÇÃO DE RESERVA
  rejectRequest(id, element)
  {
    this.spaceService.rejected({ status: false }, id).subscribe(response => {
      console.log(response);
      this.globals.bookingsRequests.splice(element, 1);
      this.router.navigate(['requests']); // <-- REMOVE ELEMENTO SE A SOLICITAÇÃO FOR REJEITADA
    });
  }

}
