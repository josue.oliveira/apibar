import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SpaceService } from '../_services/space.service';
import { UserService } from '../_services/user.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.css']
})
export class BookingDetailsComponent {

  book: any; // dados da reserva
  ownerId: any; // id do proprietário
  owner: any; // Dados do proprietário
  userId: any; // Armazena o id do usuário atual
  public isCheckOut: Boolean;

  constructor(private route: ActivatedRoute, private router: Router, private spaceService: SpaceService, private userService: UserService) { }

  ngOnInit() {

    // Pega o id do usuário
    this.userId = parseInt(JSON.parse(localStorage.getItem("currentUser")).id).toString();

    // Atribuir userId ao HEADERS
    const head = new HttpHeaders().append('userId', this.userId);

    // BUSCAR ESPAÇO PELO ID
    this.route.paramMap.subscribe(params => {

      this.spaceService.oneBook(parseInt(params.get('bookId')), head).subscribe(data => {
        this.book = data;

        this.book.forEach(element => {
          if(element.status)
          {
            element.status = "ativa";
          }
          if(element.check_out == null)
          {
            this.isCheckOut = false;
          } else { this.isCheckOut = true; }

          this.ownerId = element.userId;
        });

        // User service
        this.userService.getById(this.ownerId).subscribe(data => {
          this.owner = data;
        })
      });
    });
  }

  // Exclui uma reserva
  excluirReserva()
  {
    this.route.paramMap.subscribe(params => {
      this.spaceService.deleteBook(parseInt(params.get('bookId'))).subscribe(response => {
        console.log(response);
        console.log("Reserva excluida com sucesso!");
        this.router.navigate(['booking']);
        // window.location.reload();
      })
    })
  }

}
