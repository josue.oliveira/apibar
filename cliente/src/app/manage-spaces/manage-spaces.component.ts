import { Component, OnInit } from '@angular/core';
import { SpaceService } from '../_services/space.service';

@Component({
  selector: 'app-manage-spaces',
  templateUrl: './manage-spaces.component.html',
  styleUrls: ['./manage-spaces.component.css']
})
export class ManageSpacesComponent {

  spaces: Array<any>;

  constructor(private spaceService: SpaceService) { }

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.spaceService.getByOwner(JSON.parse(localStorage.getItem("currentUser")).id).subscribe(data => {
      this.spaces = data;
    });
  }

}
