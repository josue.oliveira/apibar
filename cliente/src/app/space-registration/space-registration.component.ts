import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';

import { SpaceService } from '../_services/space.service';
import { Router } from '@angular/router';

import {COMMA, ENTER} from '@angular/cdk/keycodes';

import {MatAutocompleteSelectedEvent, MatAutocomplete} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-space-registration',
  templateUrl: './space-registration.component.html',
  styleUrls: ['./space-registration.component.css']
})
export class SpaceRegistrationComponent {

  space: any;

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable<string[]>;
  fruits: string[] = ['Casuais'];
  allFruits: string[] = ['Aniversários', 'Casamentos', 'Corporativos', 'Reuniões', 'Formaturas', 'Festa particular', 'Baladas', 'Religiosos', 'Empresariais', 'Governamentais', 'Universitários', 'Beneficentes'];

  @ViewChild('fruitInput', {static: false}) fruitInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', {static: false}) matAutocomplete: MatAutocomplete;

  constructor(private _formBuilder: FormBuilder, private router: Router, private spaceService: SpaceService)
  {
    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => fruit ? this._filter(fruit) : this.allFruits.slice()));
  }

  ngOnInit() {
    this.space = {};
  }

  // DEFAULT POST
  create(frm: FormGroup){
    
    this.space["type"] = this.fruits; // Adiciona as tags
    this.space["userId"] = JSON.parse(localStorage.getItem("currentUser")).id; // Adiciona o ID do user atual

    this.spaceService.create(this.space).subscribe( data  => {
          console.log("Espaço cadastrado com sucesso! ", data);
          var json = JSON.stringify(data);
          sessionStorage.setItem('id_space_creation', JSON.parse(json).id);
          this.router.navigate(['manage_spaces/newspace/upload_image']);
        },
        error  => {
        
        console.log("Error", error);
        
        }
    );
    frm.reset();
  }

  // FORMS MATERIAL STEPPER
  add(event: MatChipInputEvent): void {
    // Add fruit only when MatAutocomplete is not open
    // To make sure this does not conflict with OptionSelected Event
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;

      // Add our fruit
      if ((value || '').trim()) {
        this.fruits.push(value.trim());
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }

      this.fruitCtrl.setValue(null);
    }
  }

  remove(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.fruits.push(event.option.viewValue);
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.toLowerCase().indexOf(filterValue) === 0);
  }

}
