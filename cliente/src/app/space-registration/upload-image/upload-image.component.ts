import { Component, OnInit } from '@angular/core';
import { DialogComponent } from '../../dialog/dialog.component';
import { UploadService } from '../../_services/upload.service';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.css']
})
export class UploadImageComponent implements OnInit {

  constructor(public dialog: MatDialog, public uploadService: UploadService) { }

  public openUploadDialog() {
    this.uploadService.create_space = true;
    let dialogRef = this.dialog.open(DialogComponent, { width: '50%', height: '50%' });
  }

  ngOnInit() {
  }

}
