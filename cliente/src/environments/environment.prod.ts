export const environment = {
  production: true,
  apiUrl: 'http://192.168.43.14/:3000',
  // MÉTODOS HTTP
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE'
};
