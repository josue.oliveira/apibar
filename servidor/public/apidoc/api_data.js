define({ "api": [
  {
    "type": "post",
    "url": "/login",
    "title": "User Login",
    "group": "Login",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Email address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"username\": \"email@example.com\",\n  \"password\": \"password\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Authentication Token</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User ID logged in</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n  \"token\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTMyOTUyLCJleHAiOjE1NzU5MzY1NTJ9.6pa5ptmBI700CQbClH-s6pxtucIE_xDJLzDeysRAOtE\",\n  \"id\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "notAuthorizedError",
            "description": "<p>User not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/doc.js",
    "groupTitle": "Login",
    "name": "PostLogin"
  },
  {
    "type": "get",
    "url": "/content/spaces",
    "title": "List all spaces",
    "group": "Spaces",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo",
            "description": "<p>Authorization token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "spaces",
            "description": "<p>Spaces list</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "spaces.id",
            "description": "<p>Space id</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "spaces.type",
            "description": "<p>Space Type</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "spaces.price",
            "description": "<p>Space Price</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "spaces.capacity",
            "description": "<p>Space Capacity</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "spaces.name",
            "description": "<p>Space Name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "spaces.address",
            "description": "<p>Space Address</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "spaces.city",
            "description": "<p>Space City</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "spaces.state",
            "description": "<p>City State</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "spaces.country",
            "description": "<p>Country State</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "spaces.cep",
            "description": "<p>Space ZIP code</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "spaces.description",
            "description": "<p>Space Description</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "spaces.lat",
            "description": "<p>Space latitude</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "spaces.lng",
            "description": "<p>Space longitude</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "spaces.image",
            "description": "<p>Space Image</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "users.created_at",
            "description": "<p>Register's date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "users.updated_at",
            "description": "<p>Update's date</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "spaces.userId",
            "description": "<p>Space Owner id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 3,\n      \"type\": [\n      \"Casuais\",\n      \"Festa particular\",\n      \"Universitários\"\n      ],\n      \"price\": 300,\n      \"capacity\": 200,\n      \"name\": \"Botequim\",\n      \"address\": \"Av. Rui Barbosa, 379 - Centro\",\n      \"city\": \"Santarém\",\n      \"state\": \"PA\",\n      \"country\": \"Brasil\",\n      \"cep\": \"68005260\",\n      \"description\": \"O melhor espaço para sua festa. Nosso local já dispõem de vários serviços, incluindo bebidas e aparelhos de som. Bem localizado, somos referência na cidade de Santarém.\",\n      \"lat\": -2.419401,\n      \"lng\": -54.710122,\n      \"image\": \"http://res.cloudinary.com/dacnpoiak/image/upload/v1574223249/kuditglmalxgjp16ftqt.jpg\",\n      \"createdAt\": \"2019-11-20T04:12:19.355Z\",\n      \"updatedAt\": \"2019-11-20T04:17:17.606Z\",\n      \"userId\": 4\n  },\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "notAuthorizedError",
            "description": "<p>User not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/doc.js",
    "groupTitle": "Spaces",
    "name": "GetContentSpaces"
  },
  {
    "type": "delete",
    "url": "/users/:id",
    "title": "Remove a user",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo",
            "description": "<p>Authorization token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>User id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[\n  1\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Update error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/doc.js",
    "groupTitle": "Users",
    "name": "DeleteUsersId"
  },
  {
    "type": "get",
    "url": "/users",
    "title": "List all users",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo",
            "description": "<p>Authorization token</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "users",
            "description": "<p>Users list</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "users.id",
            "description": "<p>User id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.profile_picture",
            "description": "<p>User profile picture</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.email",
            "description": "<p>User email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.name",
            "description": "<p>User name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.lastname",
            "description": "<p>User lastname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.password",
            "description": "<p>User password</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.dob",
            "description": "<p>User Date of birth</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.official_id",
            "description": "<p>User Official id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.address",
            "description": "<p>User Address</p>"
          },
          {
            "group": "Success 200",
            "type": "Enum",
            "optional": false,
            "field": "users.roles",
            "description": "<p>User Roles</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "users.created_at",
            "description": "<p>Register's date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "users.updated_at",
            "description": "<p>Update's date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"profile_picture\": null,\n      \"email\": \"example@email.com\",\n      \"name\": \"user\",\n      \"lastname\": \"lastname\",\n      \"password\": \"$argon2i$v=19$m=4096,t=3,p=1$h/9pAgEoIzviWpyNA6bPIg$3Kfj3JyWc/2SA25rpWAPzY40xpJkv/MPe1wqLpuH9s0\",\n      \"dob\": \"23/09/1996\",\n      \"official_id\": null,\n      \"address\": null,\n      \"roles\": \"customer\",\n      \"createdAt\": \"2019-10-13T04:11:09.617Z\",\n      \"updatedAt\": \"2019-10-13T04:11:09.617Z\"\n  },\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "notAuthorizedError",
            "description": "<p>User not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/doc.js",
    "groupTitle": "Users",
    "name": "GetUsers"
  },
  {
    "type": "get",
    "url": "/users/:id",
    "title": "Find a user",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo",
            "description": "<p>Authorization token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>User id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "users.id",
            "description": "<p>User id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.profile_picture",
            "description": "<p>User profile picture</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.email",
            "description": "<p>User email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.name",
            "description": "<p>User name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.lastname",
            "description": "<p>User lastname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.password",
            "description": "<p>User password</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.dob",
            "description": "<p>User Date of birth</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.official_id",
            "description": "<p>User Official id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "users.address",
            "description": "<p>User Address</p>"
          },
          {
            "group": "Success 200",
            "type": "Enum",
            "optional": false,
            "field": "users.roles",
            "description": "<p>User Roles</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "users.created_at",
            "description": "<p>Register's date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "users.updated_at",
            "description": "<p>Update's date</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 4,\n  \"profile_picture\": \"http://res.cloudinary.com/dacnpoiak/image/upload/v1574402183/j8gvrgwhekdvcriiynoe.jpg\",\n  \"email\": \"teste@email.com\",\n  \"name\": \"Josué\",\n  \"lastname\": \"Acaz\",\n  \"password\": \"$argon2i$v=19$m=4096,t=3,p=1$cRI1q69HuL3vaK3deyDzjw$ECvDLXlXq7PkCHN6JVkZm6oz0Xk6kD+Ab3a6Ltfd37M\",\n  \"dob\": \"23/09/1996\",\n  \"official_id\": null,\n  \"address\": null,\n  \"roles\": \"customer\",\n  \"createdAt\": \"2019-10-13T04:38:35.425Z\",\n  \"updatedAt\": \"2019-10-20T20:45:51.036Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "401": [
          {
            "group": "401",
            "optional": false,
            "field": "notAuthorizedError",
            "description": "<p>User not found.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./routes/doc.js",
    "groupTitle": "Users",
    "name": "GetUsersId"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Register a new user",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User lastname</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "dob",
            "description": "<p>User Date of birth</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"name\": \"Name\",\n  \"lastname\": \"Lastname\",\n  \"email\": \"email@example.com\",\n  \"password\": \"password\",\n  \"dob\": \"10/12/2019\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Enum",
            "optional": false,
            "field": "roles",
            "description": "<p>User roles</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User lastname</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User password</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "dob",
            "description": "<p>User Date of birth</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Update date</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Register date</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile_picture",
            "description": "<p>User Profile picture</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "official_id",
            "description": "<p>User Official id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>User Address</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n  \"roles\": \"customer\",\n  \"id\": 13,\n  \"name\": \"Name\",\n  \"lastname\": \"Lastname\",\n  \"email\": \"email@example.com\",\n  \"password\": \"$argon2i$v=19$m=4096,t=3,p=1$Hp9D9QPfT2pqhtmeWRVBaA$L7+japCEk1YE0AzYVLoRpfG83WzgDBexScIbl7aKCRI\",\n  \"dob\": \"10/12/2019\",\n  \"updatedAt\": \"2019-12-10T01:13:03.442Z\",\n  \"createdAt\": \"2019-12-10T01:13:03.442Z\",\n  \"profile_picture\": null,\n  \"official_id\": null,\n  \"address\": null\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Register error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/doc.js",
    "groupTitle": "Users",
    "name": "PostUsers"
  },
  {
    "type": "put",
    "url": "/users/:id",
    "title": "Update a user",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo",
            "description": "<p>Authorization token</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "id",
            "optional": false,
            "field": "id",
            "description": "<p>User id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>User name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User lastname</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Input",
          "content": "{\n  \"name\": \"Changed name\",\n  \"lastname\": \"Changed lastname\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[\n  1\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Update error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./routes/doc.js",
    "groupTitle": "Users",
    "name": "PutUsersId"
  }
] });
