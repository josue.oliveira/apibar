#!/usr/bin/env node

const express = require('express');
const bodyParser = require("body-parser");
var debug = require('debug')('servidor:server');
const path = require("path");
const cors = require('cors');

const config = require("./config/config");
const datasource = require("./config/database");

// websocket
const socketio = require('socket.io');
const http = require('http');

// Carrega as rotas
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const spacesRouter = require("./routes/spaces");
const locationsRouter = require("./routes/locations");
const authRouter = require("./routes/auth");
var cloudiRouter = require('./routes/images');
const BookingRouter = require('./routes/Booking');

const authorization = require('./services/auth');

const app = express();
const server = http.Server(app);
const io = socketio(server);

// Server Config
var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);


// SETUP DATABASE
app.config = config;
app.datasource = datasource(app);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static('uploads'));

app.use(bodyParser.json({
    limit: '5mb'
}));

app.use(bodyParser.urlencoded({ extended: false }));

// Authorization
const auth = authorization(app);
app.use(auth.initialize());
app.auth = auth;

const connectedUsers = {};

io.on('connection', socket => {
  const { userId } = socket.handshake.query;

  connectedUsers[userId] = socket.id;
});

app.use((req, res, next) => {
  req.io = io;
  req.connectedUsers = connectedUsers;

  return next();
})

// Habilita o CORS
app.use(cors());

// HABILITA ROTAS
indexRouter(app);
usersRouter(app);
spacesRouter(app);
locationsRouter(app);
authRouter(app);
cloudiRouter(app);
BookingRouter(app);

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
console.log('Server listening on port ' + port);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
  
    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
  }
  
  /**
   * Event listener for HTTP server "error" event.
   */
  
  function onError(error) {
    if (error.syscall !== 'listen') {
      throw error;
    }
  
    var bind = typeof port === 'string'
      ? 'Pipe ' + port
      : 'Port ' + port;
  
    // handle specific listen errors with friendly messages
    switch (error.code) {
      case 'EACCES':
        console.error(bind + ' requires elevated privileges');
        process.exit(1);
        break;
      case 'EADDRINUSE':
        console.error(bind + ' is already in use');
        process.exit(1);
        break;
      default:
        throw error;
    }
  }
  
  /**
   * Event listener for HTTP server "listening" event.
   */
  
  function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
      ? 'pipe ' + addr
      : 'port ' + addr.port;
    debug('Listening on ' + bind);
  }

module.exports = app;