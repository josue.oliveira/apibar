# API RESTFULL DA APLICAÇÃO EVENTL

Para instalar todas as dependência execute `npm install`, para instalar as dependências do desenvolvedor.

# SERVIDOR

Para executar o servidor rode `npm start`. O mesmo ficará disponível na em [URL](localhost:3000).

# DOCUMENTAÇÃO

Para gerar a documentação é preciso ter o yarn instalado, então rode ``yarn gendoc``
A documentação contém todas as rotas que podem ser acessada pela aplicação.