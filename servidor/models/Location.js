
module.exports = (sequelize, DataType) => {
    const Location = sequelize.define('locations', {
        check_in: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        check_out: { // Pode ser nulo
            type: DataType.STRING
        },
        totalPaid: {
            type: DataType.FLOAT,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        status: {
            type: DataType.BOOLEAN,
            defaultValue: null
        }
    });

    return Location;
};