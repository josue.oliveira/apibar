
module.exports = (sequelize, DataType) => {
    const Space = sequelize.define('spaces', {

        // TAGS
        type: { // TIPO DO EVENTO
            type: DataType.ARRAY(DataType.STRING),
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        price: { // PREÇO DE LOCAÇÃO POR DIÁRIA
            type: DataType.FLOAT,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        capacity: { // CAPACIDADE DO LOCAL
            type: DataType.INTEGER,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        name: { // NOME DO ESPAÇO
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        address: { // ENDEREÇO
            type: DataType.STRING
        },
        city: { // CIDADE
            type: DataType.STRING
        },
        state: { // ESTADO
            type: DataType.STRING
        },
        country: { // PAÍS
            type: DataType.STRING
        },
        cep: { // CÓDIGO DE ENDERAÇAMENTO POSTAL
            type: DataType.STRING
        },
        description: { // DESCRIÇÃO
            type: DataType.STRING
        },
        lat: {
            type: DataType.DOUBLE
        },
        lng: {
            type: DataType.DOUBLE
        },
        // APÓS CADASTRO, FAZER UPLOAD DA IMAGEM
        image: { // IMAGEM DE EXIBIÇÃO DO ANÚNCIO
            type: DataType.STRING
        }

    });

    return Space;
};
