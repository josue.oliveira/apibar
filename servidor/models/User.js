const argon2 = require('argon2');

module.exports = (sequelize, DataType) => {
    const User = sequelize.define('users', {
        profile_picture: {
            type: DataType.STRING
        },
        email: {
            type: DataType.STRING,
            allowNull: false,
            unique: true,
            validate: {
                isEmail: true,
                notEmpty: true
            }
        },
        name: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        lastname: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        password: {
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        dob: { // Data de nascimento
            type: DataType.STRING,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        official_id: { // Somente na hora de complementar as informações de identificação pessoal
            type: DataType.STRING
        },
        address: { // Somente na hora ....
            type: DataType.STRING
        },
        roles: {
            type: DataType.ENUM,
            values: ['customer', 'owner', 'admin'],
            defaultValue: 'customer',
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    },
    {
        hooks:{
            async beforeCreate(user){
                try{
                    const hash = await argon2.hash(user.password);
                    user.set('password', hash);
                } catch(e){
                    console.error(e);
                }
            }
        }
    });

    // Verificação
    User.verifyPassword = async (hash, password) => {
        try{
            if(await argon2.verify(hash, password)){
                return true;
            }

        } catch(e){
            console.error(e);
        }

        return false;
    }

    return User;
};