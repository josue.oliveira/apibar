// MODEL UTILIZADO PARA ARMAZENAR TODAS AS IMAGENS DE ESPAÇOS
module.exports = (sequelize, DataType) => {

    const Image = sequelize.define('images', {

        cloud_name: {
            type: DataType.STRING
        },
        cloud_url: {
            type: DataType.STRING
        },
        cloud_id: {
            type: DataType.STRING
        },
        post_date: {
            type: DataType.DATE,
            defaultValue: DataType.NOW
        }

    });

    return Image;
};
