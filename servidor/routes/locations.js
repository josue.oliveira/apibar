const LocationsController = require('../controllers/locations');

class Space{
    constructor(modelSpace, modelUser)
    {
        this.SpaceModel = modelSpace;
        this.UserModel = modelUser;
    }

    getSpace(id)
    {
        return this.SpaceModel.findOne({where: id});
    }

    getUser(id)
    {
        return this.UserModel.findOne({where: id});
    }
}

module.exports = (app) => {

    var query = new Space(app.datasource.models.spaces, app.datasource.models.users);
    const locationsController = new LocationsController(app.datasource.models.locations, app.datasource.sequelize);

    app.route('/customers/locations')
        .get( (req, res) => {
            locationsController
                .getAll()
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .post((req, res) => {
            locationsController
                .create(req.body)
                .then(rs => {

                    var booking = rs.data.dataValues;
                    var space = {};
                    var user = {};
                    var populate = {};

                    query.getSpace(booking.spaceId).then(rs => {
                        space = rs.dataValues;

                        const populate = Object.assign({}, booking, {space: space});

                        return populate;
                    })
                    .then(rs => {

                        var data = rs;

                        query.getUser(rs.userId).then(rs => {
                            user = rs.dataValues;

                            populate = Object.assign({}, data, {user: user});

                            const ownerSocket = req.connectedUsers[populate.space.userId];

                            if(ownerSocket){
                                req.io.to(ownerSocket).emit('booking_request', populate);
                            }

                            return populate;
                        })
                        .then(rs => {
                            res.json(rs);
                            res.status(rs.status);
                        })
                        .catch(error => {
                            console.error(error.message);
                            res.status(error.status);
                        });
                    })
                    .catch(error => {
                        console.error(error.message);
                        res.status(error.status);
                    });
                    
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    app.route('/customers/locations/:id')
        .get(async (req, res) => {
            locationsController
                .getById(req.params)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .put((req, res) => {
            locationsController
                .update(req.body, req.params)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .delete((req, res) => {
            locationsController
                .delete(req.params)
                .then(rs => {
                    res.json(rs.data);
                    res.status(rs.status);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

        app.route('/customers/bookings/:userId')
        .get( async (req, res) => {
                locationsController
                    .getAllBookingsByUser(req.body, req.params)
                    .then(rs => {
                        res.json(rs.data);
                    })
                    .catch(error => {
                        console.error(error.message);
                        res.status(error.status);
                    });
        });

        app.route('/customers/book/:bookId')
        .get( async (req, res) => {
                locationsController
                    .getBookingByUser(req.params, req.get('userId'))
                    .then(rs => {
                        res.json(rs.data);
                    })
                    .catch(error => {
                        console.error(error.message);
                        res.status(error.status);
                    });
        });

        app.route('/space/bookings/:spaceId')
        .get( async (req, res) => {
                locationsController
                    .getAllBookingsBySpace(req.params)
                    .then(rs => {
                        res.json(rs.data);
                    })
                    .catch(error => {
                        console.error(error.message);
                        res.status(error.status);
                    });
        });
};