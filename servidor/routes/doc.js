// LOGIN
    /**
     * @api {post} /login User Login
     * @apiGroup Login
     * @apiParam {String} username Email address
     * @apiParam {String} password Password
     * @apiParamExample {json} Input
     *    {
     *      "username": "email@example.com",
     *      "password": "password"
     *    }
     * @apiSuccess {String} token Authentication Token
     * @apiSuccess {Number} id User ID logged in
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    {
     *      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTMyOTUyLCJleHAiOjE1NzU5MzY1NTJ9.6pa5ptmBI700CQbClH-s6pxtucIE_xDJLzDeysRAOtE",
     *      "id": 1
     *    }
     * @apiErrorTitle (401) 401 Unauthorized
     * @apiError (401) notAuthorizedError User not found.
    */

// USERS
    /**
     * @api {get} /users List all users
     * @apiGroup Users
     * @apiHeader {String} Authorization='bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo' Authorization token
     * @apiSuccess {Object[]} users Users list
     * @apiSuccess {Number} users.id User id
     * @apiSuccess {String} users.profile_picture User profile picture
     * @apiSuccess {String} users.email User email
     * @apiSuccess {String} users.name User name
     * @apiSuccess {String} users.lastname User lastname
     * @apiSuccess {String} users.password User password
     * @apiSuccess {String} users.dob User Date of birth
     * @apiSuccess {String} users.official_id User Official id
     * @apiSuccess {String} users.address User Address
     * @apiSuccess {Enum} users.roles User Roles
     * @apiSuccess {Date} users.created_at Register's date
     * @apiSuccess {Date} users.updated_at Update's date
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    [
     *      {
     *          "id": 1,
     *          "profile_picture": null,
     *          "email": "example@email.com",
     *          "name": "user",
     *          "lastname": "lastname",
     *          "password": "$argon2i$v=19$m=4096,t=3,p=1$h/9pAgEoIzviWpyNA6bPIg$3Kfj3JyWc/2SA25rpWAPzY40xpJkv/MPe1wqLpuH9s0",
     *          "dob": "23/09/1996",
     *          "official_id": null,
     *          "address": null,
     *          "roles": "customer",
     *          "createdAt": "2019-10-13T04:11:09.617Z",
     *          "updatedAt": "2019-10-13T04:11:09.617Z"
     *      },
     *    ]
     * @apiErrorTitle (401) 401 Unauthorized
     * @apiError (401) notAuthorizedError User not found.
    */

    /**
     * @api {post} /users Register a new user
     * @apiGroup Users
     * @apiParam {String} name User name
     * @apiParam {String} lastname User lastname
     * @apiParam {String} email User email
     * @apiParam {String} password User password
     * @apiParam {String} dob User Date of birth
     * @apiParamExample {json} Input
     *    {
     *      "name": "Name",
     *      "lastname": "Lastname",
     *      "email": "email@example.com",
     *      "password": "password",
     *      "dob": "10/12/2019"
     *    }
     * @apiSuccess {Enum} roles User roles
     * @apiSuccess {Number} id User id
     * @apiSuccess {String} name User name
     * @apiSuccess {String} lastname User lastname
     * @apiSuccess {String} email User email
     * @apiSuccess {String} password User password
     * @apiSuccess {String} dob User Date of birth
     * @apiSuccess {Date} updated_at Update date
     * @apiSuccess {Date} created_at Register date
     * @apiSuccess {String} profile_picture User Profile picture
     * @apiSuccess {String} official_id User Official id
     * @apiSuccess {String} address User Address
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    {
     *      "roles": "customer",
     *      "id": 13,
     *      "name": "Name",
     *      "lastname": "Lastname",
     *      "email": "email@example.com",
     *      "password": "$argon2i$v=19$m=4096,t=3,p=1$Hp9D9QPfT2pqhtmeWRVBaA$L7+japCEk1YE0AzYVLoRpfG83WzgDBexScIbl7aKCRI",
     *      "dob": "10/12/2019",
     *      "updatedAt": "2019-12-10T01:13:03.442Z",
     *      "createdAt": "2019-12-10T01:13:03.442Z",
     *      "profile_picture": null,
     *      "official_id": null,
     *      "address": null
     *    }
     * @apiErrorExample {json} Register error
     *    HTTP/1.1 500 Internal Server Error
     */

    /**
     * @api {get} /users/:id Find a user
     * @apiGroup Users
     * @apiHeader {String} Authorization='bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo' Authorization token
     * @apiParam {id} id User id
     * @apiSuccess {Number} users.id User id
     * @apiSuccess {String} users.profile_picture User profile picture
     * @apiSuccess {String} users.email User email
     * @apiSuccess {String} users.name User name
     * @apiSuccess {String} users.lastname User lastname
     * @apiSuccess {String} users.password User password
     * @apiSuccess {String} users.dob User Date of birth
     * @apiSuccess {String} users.official_id User Official id
     * @apiSuccess {String} users.address User Address
     * @apiSuccess {Enum} users.roles User Roles
     * @apiSuccess {Date} users.created_at Register's date
     * @apiSuccess {Date} users.updated_at Update's date
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    {
     *      "id": 4,
     *      "profile_picture": "http://res.cloudinary.com/dacnpoiak/image/upload/v1574402183/j8gvrgwhekdvcriiynoe.jpg",
     *      "email": "teste@email.com",
     *      "name": "Josué",
     *      "lastname": "Acaz",
     *      "password": "$argon2i$v=19$m=4096,t=3,p=1$cRI1q69HuL3vaK3deyDzjw$ECvDLXlXq7PkCHN6JVkZm6oz0Xk6kD+Ab3a6Ltfd37M",
     *      "dob": "23/09/1996",
     *      "official_id": null,
     *      "address": null,
     *      "roles": "customer",
     *      "createdAt": "2019-10-13T04:38:35.425Z",
     *      "updatedAt": "2019-10-20T20:45:51.036Z"
     *    }
     * @apiErrorTitle (401) 401 Unauthorized
     * @apiError (401) notAuthorizedError User not found.
     */

    /**
     * @api {put} /users/:id Update a user
     * @apiGroup Users
     * @apiHeader {String} Authorization='bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo' Authorization token
     * @apiParam {id} id User id
     * @apiParam {String} name User name
     * @apiParam {String} lastname User lastname
     * @apiParamExample {json} Input
     *    {
     *      "name": "Changed name",
     *      "lastname": "Changed lastname"
     *    }
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    [
     *      1
     *    ]
     * @apiErrorExample {json} Update error
     *    HTTP/1.1 500 Internal Server Error
     */

    /**
     * @api {delete} /users/:id Remove a user
     * @apiGroup Users
     * @apiHeader {String} Authorization='bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo' Authorization token
     * @apiParam {id} id User id
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    [
     *      1
     *    ]
     * @apiErrorExample {json} Update error
     *    HTTP/1.1 500 Internal Server Error
     */

// SPACES
/**
     * @api {get} /content/spaces List all spaces
     * @apiGroup Spaces
     * @apiHeader {String} Authorization='bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImlkIjo0fSwiaWF0IjoxNTc1OTM3MTUwLCJleHAiOjE1NzU5NDA3NTB9.GTOaY8PEQDk2fOHa-G_nHESe4451BYjvKdxfUEfZlOo' Authorization token
     * @apiSuccess {Object[]} spaces Spaces list
     * @apiSuccess {Number} spaces.id Space id
     * @apiSuccess {Array} spaces.type Space Type
     * @apiSuccess {Number} spaces.price Space Price
     * @apiSuccess {Number} spaces.capacity Space Capacity
     * @apiSuccess {String} spaces.name Space Name
     * @apiSuccess {String} spaces.address Space Address
     * @apiSuccess {String} spaces.city Space City
     * @apiSuccess {String} spaces.state City State
     * @apiSuccess {String} spaces.country Country State
     * @apiSuccess {String} spaces.cep Space ZIP code
     * @apiSuccess {String} spaces.description Space Description
     * @apiSuccess {Number} spaces.lat Space latitude
     * @apiSuccess {Number} spaces.lng Space longitude
     * @apiSuccess {String} spaces.image Space Image
     * @apiSuccess {Date} users.created_at Register's date
     * @apiSuccess {Date} users.updated_at Update's date
     * @apiSuccess {Number} spaces.userId Space Owner id
     * @apiSuccessExample {json} Success
     *    HTTP/1.1 200 OK
     *    [
     *      {
     *          "id": 3,
     *          "type": [
     *          "Casuais",
     *          "Festa particular",
     *          "Universitários"
     *          ],
     *          "price": 300,
     *          "capacity": 200,
     *          "name": "Botequim",
     *          "address": "Av. Rui Barbosa, 379 - Centro",
     *          "city": "Santarém",
     *          "state": "PA",
     *          "country": "Brasil",
     *          "cep": "68005260",
     *          "description": "O melhor espaço para sua festa. Nosso local já dispõem de vários serviços, incluindo bebidas e aparelhos de som. Bem localizado, somos referência na cidade de Santarém.",
     *          "lat": -2.419401,
     *          "lng": -54.710122,
     *          "image": "http://res.cloudinary.com/dacnpoiak/image/upload/v1574223249/kuditglmalxgjp16ftqt.jpg",
     *          "createdAt": "2019-11-20T04:12:19.355Z",
     *          "updatedAt": "2019-11-20T04:17:17.606Z",
     *          "userId": 4
     *      },
     *    ]
     * @apiErrorTitle (401) 401 Unauthorized
     * @apiError (401) notAuthorizedError User not found.
    */