const SpacesController = require('../controllers/spaces');
var multer = require('../middleware/multer');

module.exports = (app) => {

    const spacesController = new SpacesController(app.datasource.models.spaces, app.datasource.sequelize);

    app.route('/content/spaces')
        .get((req, res) => {
            spacesController
                .getAll()
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .post(/*auth.authorize, auth.isProvider,*/ (req, res) => { // É necessário estar autenticado e ser um provedor
            spacesController
                .create(req.body)
                .then(rs => {
                    
                    res.json(rs.data);
                    res.status(rs.status);

                    // Ver o que fazer depois que uma locação for efetivada
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    app.route('/content/spaces/:id')
        .get(async (req, res) => {
            spacesController
                .getById(req.params)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .put(/*auth.authorize, auth.isProvider,*/ (req, res) => {
            spacesController
                .update(req.body, req.params)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .delete(/*auth.authorize, auth.isProvider,*/ (req, res) => {
            spacesController
                .delete(req.params)
                .then(rs => {
                    res.json(rs.data);
                    res.status(rs.status);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    app.route('/upload/space_picture')
        .put(multer.any(), (req, res) => {
            spacesController
                .upload(req.body, req.files)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    app.route('/owner/spaces/:userId')
        .get(async (req, res) => {
            spacesController
                .getByOwner(req.params)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    app.route('/filter/spaces')
        .get(async (req, res) => {
            await spacesController
                .filter(req.query)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });
};