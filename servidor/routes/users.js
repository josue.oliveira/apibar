const UsersController = require('../controllers/users');
const emailService = require('../services/email');
var multer = require('../middleware/multer');

module.exports = (app) => {

    const usersController = new UsersController(app.datasource.models.users, app.datasource.sequelize);

    app.route('/users')
        .get(app.auth.authenticate(), /*auth.isAdmin,*/ (req, res) => {
            usersController
                .getAll()
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .post((req, res) => {
            usersController
                .create(req.body)
                .then(rs => {
                    
                    res.json(rs.data);
                    res.status(rs.status);

                    // ENVIA EMAIL
                    if(rs.status === 201){
                        // emailService.send( req.body.email, 'Bem vindo a plataforma EvenL', global.EMAIL_TMPL.replace('{0}', req.body.name));
                    }
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    app.route('/users/:id')
        .all(app.auth.authenticate())
        .get(async (req, res) => {
            usersController
                .getById(req.params)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .put((req, res) => {
            usersController
                .update(req.body, req.params)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
        .delete((req, res) => {
            usersController
                .delete(req.params)
                .then(rs => {
                    res.json(rs.data);
                    res.status(rs.status);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    app.route('/upload/profile_picture')
        .put(multer.any(), (req, res) => {
            usersController
            .upload(req.body, req.files)
                .then(rs => {
                    res.json(rs.data);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });
};