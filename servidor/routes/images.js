var ImageController = require('../controllers/images');
var upload = require('../middleware/multer');

module.exports = (app) => {
    
    const imageController = new ImageController(app.datasource.models.images, app.datasource.sequelize);

    // GET ALL IMAGES
    app.route('/images')
        .get((req, res) => {
            imageController
            .getAll()
            .then(rs => {
                res.json(rs.data);
            })
            .catch(error => {
                console.error(error.message);
                res.status(error.status);
            });
        })

    // RETORNA AS IMAGES DO ESPAÇO
    app.route('/images/space/:spaceId')
        .get((req, res) => {
            imageController
            .getBySpace(req.params)
            .then(rs => {
                res.json(rs.data);
                console.log(rs.data);
            })
            .catch(error => {
                console.error(error.message);
                res.status(error.status);
            });
        })
        .delete(/*auth.authorize, auth.isProvider,*/ (req, res) => {
            imageController
                .delete(req.params)
                .then(rs => {
                    res.json(rs.data);
                    res.status(rs.status);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    // UPLOAD IMAGES
    app.route('/images/uploads')
        .post(upload.any(), (req, res) => {
            imageController
                .upload(req.body, req.files)
                .then(rs => {
                    res.json(rs.data);
                    res.status(rs.status);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });
}