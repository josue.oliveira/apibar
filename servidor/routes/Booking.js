
const ApprovalController = require('../controllers/ApprovalController');
const RejectionController = require('../controllers/RejectionController');

module.exports = (app) => {
    const approvalController = new ApprovalController(app.datasource.models.locations, app.datasource.models.spaces);
    const rejectionController = new RejectionController(app.datasource.models.locations, app.datasource.models.spaces);

    app.route('/spaces/:id/approvals')
        .put((req, res) => {
            approvalController
                .store(req.body, req.params)
                .then(rs => {

                    // O usuário que solicitou a reserva
                    const bookingUserSocket = req.connectedUsers[rs.userId];

                    if(bookingUserSocket){
                        req.io.to(bookingUserSocket).emit('booking_response', rs);
                    }
                    
                    res.json(rs);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        });

    app.route('/spaces/:id/rejections')
        .put((req, res) => {
            rejectionController
                .store(req.body, req.params)
                .then(rs => {

                    // O usuário que solicitou a reserva
                    const bookingUserSocket = req.connectedUsers[rs.userId];

                    if(bookingUserSocket){
                        req.io.to(bookingUserSocket).emit('booking_response', rs);
                    }

                    res.json(rs);
                })
                .catch(error => {
                    console.error(error.message);
                    res.status(error.status);
                });
        })
}