const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');

var database = null;
var modelsByName = []; // Guarda os models para fazer as associações

const loadModels = (sequelize) => {
    const dir = path.join(__dirname, '../models');
    const models = [];

    fs.readdirSync(dir).forEach(file => { // Percorre todos os models que serão criados
        const modelPath = path.join(dir, file);
        const model = sequelize.import(modelPath);
        models[model.name] = model; // A cada iteração essa variável vai ter o nome de um model

        // Pega os models
        modelsByName.push(models[model.name]); // Agora eu coloco no array os nomes
        
    });

    return models;
};

module.exports = (app) => {
    if(!database){
        const config = app.config;

        const sequelize = new Sequelize(
            config.database,
            config.username,
            config.password,
            config.params
        );

        database = {
            sequelize,
            Sequelize,
            models: {}
        };
        
        database.models = loadModels(sequelize);

        //console.log(modelsByName[0]); // images
        //console.log(modelsByName[1]); // locations
        //console.log(modelsByName[2]); // spaces
        //console.log(modelsByName[3]); // users

        // AGORA VEM AS ASSOCIAÇÕES
        // RE01: Customers --> Locations 1:N | Locations --> Customers N:1
        modelsByName[3].hasMany(modelsByName[1]);
        modelsByName[1].belongsTo(modelsByName[3]);

        // RE02: Owners --> Spaces 1:N | Spaces --> Owners N:1
        modelsByName[3].hasMany(modelsByName[2]);
        modelsByName[2].belongsTo(modelsByName[3]);

        // RE03: Spaces --> Locations 1:N | Locations --> Spaces N:1
        modelsByName[2].hasMany(modelsByName[1]);
        modelsByName[1].belongsTo(modelsByName[2]);

        // RE04: Spaces --> Images 1: N
        modelsByName[2].hasMany(modelsByName[0]);
        modelsByName[0].belongsTo(modelsByName[2]);

        sequelize.sync().done(() => database);
    }

    return database;
};