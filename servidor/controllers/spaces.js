var cloud = require('../config/cloudinaryConfig');
const HttpStatus = require("http-status");

const defaultResponse = (data, status = HttpStatus.OK) => ({
    data,
    status
});

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => ({
    message,
    status
}, status);

class SpacesController{

    constructor(modelSpace, sequelize){
        this.Spaces = modelSpace;
        this.object = sequelize;
    }

    getAll(){
        return this.Spaces
                .findAll({})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

    getById(params){
        return this.Spaces
                .findOne({where: params})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

    getByOwner(params){
        return this.object.query('select * from spaces where "userId"='+params.userId,
                { type: this.object.QueryTypes.SELECT})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

    create(data) {
        return this.Spaces
                .create(data)
                .then(rs => defaultResponse(rs, HttpStatus.CREATED))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    update(data, params){
        return this.Spaces
                .update(data, { where: params })
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    delete(params){
        return this.Spaces
                .destroy({where: params})
                .then(rs => defaultResponse(rs, HttpStatus.NO_CONTENT))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    upload(data, file) {
        
        var imageDetails = {
            cloud_name: data.imageName,
            cloud_url: file[0].path,
            cloud_id: '',
            spaceId: data.spaceId
        }

        return cloud.uploads(imageDetails.cloud_url).then( (result) => {

            return imageDetails = {
                cloud_name: data.imageName,
                cloud_url: result.url,
                cloud_id: result.id,
                spaceId: data.spaceId
            }
        })
        .then((result) => {

            return this.object.query("UPDATE spaces set image='"+result.cloud_url+"' where id="+result.spaceId,
            { type: this.object.QueryTypes.UPDATE });
            
        })
        .then(rs => defaultResponse(rs))
        .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    // Faz o filtro dos espaços
    filter(queryparams)
    {
        var request;

        // LOCALIZAÇÃO E CATEGORIA FORNECIDOS
        if(!queryparams.check_in && !queryparams.check_out && queryparams.city && queryparams.category)
        {
            request = "select * from spaces where city='"+queryparams.city+
            "' and state='"+queryparams.state+
            "' and country ='"+queryparams.country+
            "' and '"+queryparams.category+"'=any(type);";
        }

        // APENAS A LOCALIZAÇÃO É FORNECIDA
        else if(!queryparams.check_in && !queryparams.check_out && !queryparams.category && queryparams.city)
        {
            request = "select * from spaces where city='"+queryparams.city+
            "' and state='"+queryparams.state+
            "' and country ='"+queryparams.country+"';";
        }

        // APENAS A CATEGORIA É FORNECIDA
        else if(!queryparams.check_in && !queryparams.check_out && !queryparams.city && queryparams.category)
        {
            request = "select * from spaces where '"+queryparams.category+"'=any(type);";
        }

        // APENAS O PERÍODO É FORNECIDO
        else if(queryparams.check_in && queryparams.check_out && !queryparams.city && !queryparams.category)
        {
            request = "select spaces.id, spaces.type, spaces.price, spaces.capacity, "
            +"spaces.name, spaces.address, spaces.city, spaces.state, "
            +"spaces.country, spaces.cep, spaces.description, "
            +"spaces.lat, spaces.lng, spaces.image "
            +'from spaces left join locations on (spaces.id=locations."spaceId") '
            +"where locations.check_in is null "
                +"or not (locations.check_in >= '"+queryparams.check_in+"' and locations.check_out <= '"+queryparams.check_out+"' "
                    +"or locations.check_in <= '"+queryparams.check_in+"' and locations.check_out >= '"+queryparams.check_out+"' "
                        +"or '"+queryparams.check_in+"' BETWEEN locations.check_in AND locations.check_out"+" "
                    +"or '"+queryparams.check_out+"' BETWEEN locations.check_in AND locations.check_out);";
        }

        // LOCALIZAÇÃO E PERÍODO SÃO FORNECIDOS
        else if(!queryparams.category && queryparams.city && queryparams.check_in && queryparams.check_out)
        {
            request = "select spaces.id, spaces.type, spaces.price, spaces.capacity, "
            +"spaces.name, spaces.address, spaces.city, spaces.state, "
            +"spaces.country, spaces.cep, spaces.description, "
            +"spaces.lat, spaces.lng, spaces.image "
            +'from spaces left join locations on (spaces.id=locations."spaceId") '
            +"where locations.check_in is null "
                +"or not (locations.check_in >= '"+queryparams.check_in+"' and locations.check_out <= '"+queryparams.check_out+"' "
                    +"or locations.check_in <= '"+queryparams.check_in+"' and locations.check_out >= '"+queryparams.check_out+"' "
                        +"or '"+queryparams.check_in+"' BETWEEN locations.check_in AND locations.check_out"+" "
                    +"or '"+queryparams.check_out+"' BETWEEN locations.check_in AND locations.check_out)"+" "
                +"and spaces.city='"+queryparams.city+"' "
                +"and spaces.state='"+queryparams.state+"' "
                +"and spaces.country='"+queryparams.country+"';";
        }

        // PERÍODO E CATEGORIA SÃO FORNECIDOS
        else if(queryparams.check_in && queryparams.check_out && queryparams.category && !queryparams.city)
        {
            request = "select spaces.id, spaces.type, spaces.price, spaces.capacity, "
            +"spaces.name, spaces.address, spaces.city, spaces.state, "
            +"spaces.country, spaces.cep, spaces.description, "
            +"spaces.lat, spaces.lng, spaces.image "
            +'from spaces left join locations on (spaces.id=locations."spaceId") '
            +"where locations.check_in is null "
                +"or not (locations.check_in >= '"+queryparams.check_in+"' and locations.check_out <= '"+queryparams.check_out+"' "
                    +"or locations.check_in <= '"+queryparams.check_in+"' and locations.check_out >= '"+queryparams.check_out+"' "
                        +"or '"+queryparams.check_in+"' BETWEEN locations.check_in AND locations.check_out"+" "
                    +"or '"+queryparams.check_out+"' BETWEEN locations.check_in AND locations.check_out)"+" "
                +"and '"+queryparams.category+"'=any(spaces.type);";
        }

        // LOCALIZAÇÃO, PERÍODO E CATEGORIA FORNECIDOS (FILTRO COMPLETO)
        else if(queryparams.check_in && queryparams.check_out && queryparams.category && queryparams.city)
        {
            request = "select spaces.id, spaces.type, spaces.price, spaces.capacity, "
            +"spaces.name, spaces.address, spaces.city, spaces.state, "
            +"spaces.country, spaces.cep, spaces.description, "
            +"spaces.lat, spaces.lng, spaces.image "
            +'from spaces left join locations on (spaces.id=locations."spaceId") '
            +"where locations.check_in is null "
                +"or not (locations.check_in >= '"+queryparams.check_in+"' and locations.check_out <= '"+queryparams.check_out+"' "
                    +"or locations.check_in <= '"+queryparams.check_in+"' and locations.check_out >= '"+queryparams.check_out+"' "
                        +"or '"+queryparams.check_in+"' BETWEEN locations.check_in AND locations.check_out"+" "
                    +"or '"+queryparams.check_out+"' BETWEEN locations.check_in AND locations.check_out)"+" "
                +"and spaces.city='"+queryparams.city+"' "
                +"and spaces.state='"+queryparams.state+"' "
                +"and spaces.country='"+queryparams.country+"' "
                +"and '"+queryparams.category+"'=any(spaces.type);";
        }

        // CASO NENHUM
        else{
            return ({ message: 'Filter error' });
        }

        return this.object.query(request, { type: this.object.QueryTypes.SELECT})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
        
    }

}

module.exports = SpacesController;