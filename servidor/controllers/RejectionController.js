class RejectionController{
    constructor(modelLocation, modelSpace){
        this.Locations = modelLocation;
        this.Spaces = modelSpace;
    }

    async store(data, params){

        // Atualiza status da reserva
        await this.Locations.update(data, { where: params });

        var space = {}; var location = {}; var populate = {};

        this.getLocation(params).then(rs => {
            location = rs.dataValues;
        })

        // Retorna populate
        return this.getSpace(location.spaceId).then(rs => {
            space = rs.dataValues;
            return space;
        })
        .then(rs => {
            populate = Object.assign({}, location, {space: rs});
            return populate;
        });
    }

    async getLocation(id)
    {
        return this.Locations.findOne({where: id});
    }

    async getSpace(id)
    {
        return this.Spaces.findOne({where: id});
    }
}

module.exports = RejectionController;