const HttpStatus = require("http-status");
const SpacesController = require('../controllers/spaces');

const defaultResponse = async (data, status = HttpStatus.OK) => ({
    data,
    status
});

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => ({
    message,
    status
}, status);

class LocationsController{

    constructor(modelLocation, sequelize){
        this.Locations = modelLocation;
        this.object = sequelize;
    }

    getAll(){
        return this.Locations
                .findAll({})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

    getById(params){
        return this.Locations
                .findOne({where: params})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

    create(data) {
        return this.Locations
                .create(data)
                .then(rs => defaultResponse(rs, HttpStatus.CREATED))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    async spacePopulate(id)
    {
        var data = await this.spacesController.getById(id);
        return data;
    }

    update(data, params){
        return this.Locations
                .update(data, { where: params })
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    delete(params){
        return this.Locations
                .destroy({where: params})
                .then(rs => defaultResponse(rs, HttpStatus.NO_CONTENT))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    getBookingByUser(params, data){ // Método recebe como parâmetro o id da reserva e como data o id do usuário ao qual está vinculada a reserva
        
        return this.object.query('SELECT'
        + ' locations.check_in,'
        + ' locations.check_out,'
        + ' locations."totalPaid",'
        + ' locations.status,'
        + ' spaces.name,'
        + ' spaces.address,'
        + ' spaces.city,'
        + ' spaces.state,'
        + ' spaces."userId"' // Esse dado retornado se refere à chave estrangeira que faz o relacionamento entre a tabela de espaços e provedores
        + ' FROM locations'
        + ' INNER JOIN spaces ON (locations."spaceId" = spaces.id)'
        + ' where locations.id='+params.bookId+' and locations."userId"='+data+';',
       { type: this.object.QueryTypes.SELECT})
       .then(rs => defaultResponse(rs))
       .catch(e => errorResponse(e.message));
    }

    // Retorna todas as reservas feitas por determinado usuário
    getAllBookingsByUser(data, params){
        /*console.log('SELECT'
        +' locations.check_in,'
        +' locations.check_out,'
        +' locations."totalPaid",'
        +' locations.status,'
        +' spaces.name,'
        +' spaces.address,'
        +' spaces.city,'
        +' spaces.state'
        +' FROM locations INNER JOIN spaces ON (locations."spaceId" = spaces.id)'
        +' where locations."userId"='+4+';');*/

        // OUTRA ABORDAGEM
        /*return this.Locations.findAll({
            include: [{
                model: Spaces,
                where: {id: 4}
            }]
        })
        .then(rs => defaultResponse(rs))
        .catch(e => errorResponse(e.message));
        */

       return this.object.query('SELECT'
       +' locations.id,'
       +' locations.check_in,'
       +' locations.check_out,'
       +' locations."totalPaid",'
       +' locations.status,'
       +' spaces.name,'
       +' spaces.address,'
       +' spaces.city,'
       +' spaces.state'
       +' FROM locations INNER JOIN spaces ON (locations."spaceId" = spaces.id)'
       +' where locations."userId"='+params.userId+';',
       { type: this.object.QueryTypes.SELECT})
       .then(rs => defaultResponse(rs))
       .catch(e => errorResponse(e.message));
    }

    getAllBookingsBySpace(params){
        return this.object.query('select * from locations where "spaceId"='+params.spaceId,
                { type: this.object.QueryTypes.SELECT})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

}

module.exports = LocationsController;