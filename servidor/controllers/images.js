var cloud = require('../config/cloudinaryConfig');
const HttpStatus = require("http-status");

const defaultResponse = (data, status = HttpStatus.OK) => ({
    data,
    status
});

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => ({
    message,
    status
}, status);

class ImageController{
    constructor(modelImage, sequelize){
        this.Images = modelImage;
        this.object = sequelize;
    }

    getAll(){

        // CONSULTA BRUTA
        /*return this.object.query('SELECT * from "imageUploads"', { type: this.object.QueryTypes.SELECT })
        .then(rs => defaultResponse(rs))
        .catch(e => errorResponse(e.message));*/

        // CONSULTA NORMAL
        return this.Images
            .findAll({})
            .then(rs => defaultResponse(rs))
            .catch(e => errorResponse(e.message));
    }

    getBySpace(params){
        return this.object.query('select * from images where "spaceId"='+params.spaceId,
                { type: this.object.QueryTypes.SELECT})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

    upload(data, file) {
        
        var imageDetails = {
            cloud_name: data.imageName,
            cloud_url: file[0].path,
            cloud_id: '',
            spaceId: data.spaceId
        }

        return cloud.uploads(imageDetails.cloud_url).then( (result) => {

            return imageDetails = {
                cloud_name: data.imageName,
                cloud_url: result.url,
                cloud_id: result.id,
                spaceId: data.spaceId
            }
        })
        .then((result) => {

            // PUT DE FORMA BRUTA
            /*return this.Images.update(
                { 'photos': this.object.fn('array_append', this.object.col('photos'), result.cloudImage) },
                { 'where': { 'id': result.spaceId } }
            )*/

            // DEFAULT POST
            return this.Images.create(result);
        })
        .then(rs => defaultResponse(rs, HttpStatus.CREATED))
        .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }
    
    delete(params){
        return this.Spaces
                .destroy({where: params})
                .then(rs => defaultResponse(rs, HttpStatus.NO_CONTENT))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }
}

module.exports = ImageController;