const HttpStatus = require("http-status");
const ValidationContract = require('../validators/fluent');
var cloud = require('../config/cloudinaryConfig');

let contract = new ValidationContract();

const defaultResponse = (data, status = HttpStatus.OK) => ({ data, status });

const errorResponse = (message, status = HttpStatus.BAD_REQUEST) => ({
    message,
    status
}, status);

class UsersController{

    constructor(modelUser, sequelize){
        this.Users = modelUser;
        this.object = sequelize;
    }

    getAll(){
        return this.Users
                .findAll({})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

    getById(params){
        return this.Users
                .findOne({where: params})
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message));
    }

    create(data) {

        contract.isEmail(data.email, 'E-mail inválido!');
        contract.hasMinLen(data.name, 3, 'O nome deve conter no mínimo 3 caracteres!');
        contract.hasMinLen(data.lastname, 3, 'O sobrenome deve conter no mínimo 3 caracteres!');
        contract.hasMinLen(data.password, 6, 'A senha deve conter no mínimo 6 caraceteres!');
        // Contract dob

        // Se a validação não for aprovada, retornar erro
        if(!contract.isValid())
        {
            // VER O QUE FAZER AQUI
        }

        return this.Users
                .create(data)
                .then(rs => defaultResponse(rs, HttpStatus.CREATED))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    update(data, params){
        return this.Users
                .update(data, { where: params })
                .then(rs => defaultResponse(rs))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    delete(params){
        return this.Users
                .destroy({where: params})
                .then(rs => defaultResponse(rs, HttpStatus.NO_CONTENT))
                .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    upload(data, file) {

        var imageDetails = {
            cloud_name: data.imageName,
            cloud_url: file[0].path,
            cloud_id: '',
            userId: data.userId
        }

        return cloud.uploads(imageDetails.cloud_url).then( (result) => {

            return imageDetails = {
                cloud_name: data.imageName,
                cloud_url: result.url,
                cloud_id: result.id,
                userId: data.userId
            }
        })
        .then((result) => {
            return this.object.query("UPDATE users set profile_picture='"+result.cloud_url+"' where id="+result.userId,
            { type: this.object.QueryTypes.UPDATE });
            
        })
        .then(rs => defaultResponse(rs))
        .catch(e => errorResponse(e.message, HttpStatus.UNPROCESSABLE_ENTITY));
    }

    // LOGIN
    async signin(data){
        const response = {
            login: {
                id: null,
                isValid: false,
                message: "login inválido",
            }
        };

        if(data.username && data.password){
            const email = data.username;
            const password = data.password;

            try{

                const result = await this.Users.findOne({where: {email}});
                const user = await result;
   
                if(user){
                   const isPassword = await this.Users.verifyPassword(user.password, password);
   
                   if(isPassword){
                       response.login.id = user.id;
                       response.login.isValid = isPassword;
                       response.login.message = "logado com sucesso!";
                       response.login.email = user.email;
                       response.login.name = user.name;
                       response.login.roles = user.roles;
   
                       return response;
                   } // end if
                } // end if

            } catch(e){
                console.error(e);
            }

        } // end if

        return response;
    }

}

module.exports = UsersController;